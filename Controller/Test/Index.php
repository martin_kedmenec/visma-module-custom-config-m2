<?php

declare(strict_types=1);

namespace Unit1\CustomConfig\Controller\Test;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\RawFactory;
use Unit1\CustomConfig\Model\Config;

/**
 * Class Index
 */
class Index implements HttpGetActionInterface
{
    /**
     * @var Config $config
     */
    private Config $config;

    /**
     * @var RawFactory $rawFactory
     */
    private RawFactory $rawFactory;

    /**
     * Index constructor
     *
     * @param Config $config
     * @param RawFactory $rawFactory
     */
    public function __construct(
        Config $config,
        RawFactory $rawFactory
    ) {
        $this->config = $config;
        $this->rawFactory = $rawFactory;
    }

    /**
     * @return Raw
     */
    public function execute(): Raw
    {
        $storeId = 3;
        $storeWelcomeMsg = $this->config->get('messages/' . $storeId . '/message');

        $result = $this->rawFactory->create();
        $result->setContents($storeWelcomeMsg);

        return $result;
    }
}
