<?php

declare(strict_types=1);

namespace Unit1\CustomConfig\Model\Config;

use DOMDocument;
use DOMNode;
use DOMXPath;
use InvalidArgumentException;
use Magento\Framework\Config\ConverterInterface;

/**
 * Class Converter
 */
class Converter implements ConverterInterface
{
    /**
     * Convert dom node tree to array
     *
     * @param DOMDocument $source
     * @return array
     * @throws InvalidArgumentException
     */
    public function convert($source): array
    {
        $output = [];

        $xpath = new DOMXPath($source);
        $messages = $xpath->evaluate('/config/welcome_message');
        /** @var DOMNode $messageNode */
        foreach ($messages as $messageNode) {
            $storeId = $this->getAttributeValue($messageNode, 'store_id');

            $data = [];
            /** @var DOMNode $childNode */
            foreach ($messageNode->childNodes as $childNode) {
                $data = ['message' => $childNode->nodeValue];
            }

            $output['messages'][$storeId] = $data;
        }

        return $output;
    }

    /**
     * @param DOMNode $input
     * @param string $attributeName
     * @param null|string $default
     * @return null|string
     */
    private function getAttributeValue(DOMNode $input, string $attributeName, string $default = null): ?string
    {
        $node = $input->attributes->getNamedItem($attributeName);

        return $node ? $node->nodeValue : $default;
    }
}
