<?php

declare(strict_types=1);

namespace Unit1\CustomConfig\Model;

use Magento\Framework\Config\CacheInterface;
use Magento\Framework\Config\Data;
use Magento\Framework\Config\ReaderInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Config
 */
class Config extends Data
{
    /**
     * Config constructor
     *
     * @param ReaderInterface $reader
     * @param CacheInterface $cache
     * @param null|string $cacheId
     * @param null|SerializerInterface $serializer
     */
    public function __construct(
        ReaderInterface $reader,
        CacheInterface $cache,
        string $cacheId = null,
        SerializerInterface $serializer = null
    ) {
        parent::__construct($reader, $cache, $cacheId, $serializer);
    }
}
